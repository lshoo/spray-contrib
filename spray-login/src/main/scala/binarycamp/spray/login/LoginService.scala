package binarycamp.spray.login

import binarycamp.spray.pimp._
import binarycamp.spray.session._
import scala.concurrent.{ ExecutionContext, Future }
import scala.language.{ implicitConversions, postfixOps }
import spray.http.StatusCodes.Found
import spray.http.Uri.{ Path, Query }
import spray.http._
import spray.routing.AuthenticationFailedRejection.CredentialsMissing
import spray.routing._
import spray.routing.directives.AuthMagnet

trait LoginService[U] {
  def userService: UserService[U]

  def loginPath: Path

  def withLoginPath(path: Path): LoginService[U]

  def loginPage: LoginPageRoute

  def withLoginPage(template: LoginPageRoute): LoginService[U]

  def defaultReturnUri: Uri

  def withDefaultReturnUri(uri: Uri): LoginService[U]

  def route: Route

  def authenticator: LoginAuthenticator[U]

  def redirectToLoginPage(returnUri: Option[Uri]): Route

  def logout(): Directive0
}

object LoginService {
  def apply[U](userService: UserService[U], loginPage: LoginPageRoute)(implicit settings: LoginSettings,
                                                                       baker: CookieBaker,
                                                                       ec: ExecutionContext): LoginService[U] =
    new LoginServiceImpl[U](userService, settings.loginPath, loginPage, settings.defaultReturnUri)

  implicit def toAuthMagnet[U](service: LoginService[U])(implicit ec: ExecutionContext): AuthMagnet[UserInfo[U]] =
    AuthMagnet.fromContextAuthenticator(service.authenticator)
}

private case class LoginServiceImpl[U](val userService: UserService[U], val loginPath: Path, val loginPage: LoginPageRoute,
                                       val defaultReturnUri: Uri)(implicit baker: CookieBaker, ec: ExecutionContext)
    extends LoginService[U] with Directives with SessionDirectives {

  val loginRejectionHandler = RejectionHandler {
    case _ ⇒ redirect(Uri().withPath(loginPath).withQuery("error" -> "failed"), Found)
  }

  val route =
    path(loginPath.asMatcher) {
      get {
        parameter("error"?, "return_url".as[Uri] ? defaultReturnUri) { (error, returnUri) ⇒
          if (error.isDefined) {
            loginPage(error)
          } else {
            newSession("login_return_uri" -> returnUri) {
              loginPage(None)
            }
          }
        }
      } ~ post {
        handleRejections(loginRejectionHandler) {
          formFields("login", "password") { (login, password) ⇒
            onSuccess(userService.authenticate(login, password)) {
              case Some(userInfo) ⇒ redirectToReturnUri(userInfo)
              case None           ⇒ reject(AuthenticationFailedRejection(CredentialsMissing, List()))
            }
          }
        }
      }
    }

  def redirectToReturnUri(userInfo: UserInfo[U]): Route =
    session("login_return_uri".as[Uri]) { returnUri ⇒
      newSession("principal" -> userInfo.id) {
        redirect(returnUri, Found)
      }
    }

  def logout(): Directive0 = removeSession()

  override val authenticator: LoginAuthenticator[U] = { ctx ⇒
    CookieBaker.attribute[String](ctx.request, "principal") match {
      case Some(id) ⇒ userService.findById(id).map {
        case Some(userInfo) ⇒ Right(userInfo)
        case _              ⇒ Left(AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsMissing, List()))
      }
      case None ⇒ Future.successful(Left(AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsMissing, List())))
    }
  }

  override def redirectToLoginPage(returnUri: Option[Uri]): Route = {
    val query = Query() +? ("return_url" -> returnUri.map(ToStringSerializer(_)))
    redirect(Uri().withPath(Path / loginPath).withQuery(query), Found)
  }

  override def withLoginPath(path: Path): LoginService[U] = copy(loginPath = path)
  override def withLoginPage(template: LoginPageRoute): LoginService[U] = copy(loginPage = template)
  override def withDefaultReturnUri(uri: Uri): LoginService[U] = copy(defaultReturnUri = uri)
}
