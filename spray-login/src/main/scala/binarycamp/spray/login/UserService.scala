package binarycamp.spray.login

import scala.concurrent.Future

trait UserService[U] {
  def authenticate(login: String, password: String): Future[Option[UserInfo[U]]]
  def findById(id: String): Future[Option[UserInfo[U]]]
}

case class UserInfo[U](id: String, user: U)
