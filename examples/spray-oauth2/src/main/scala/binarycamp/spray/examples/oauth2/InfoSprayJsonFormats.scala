package binarycamp.spray.examples.oauth2

import binarycamp.spray.common.DefaultSprayJsonFormats

trait InfoSprayJsonFormats extends DefaultSprayJsonFormats {
  implicit val UserJsonFormat = jsonFormat2(User)
  implicit val InfoJsonFormat = jsonFormat2(SecretInfo)
}
