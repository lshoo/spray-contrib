package binarycamp.spray.examples.data

import binarycamp.spray.common.ErrorCodeRegistry

object Errors extends ErrorCodeRegistry {
  val BookNotFound = errorCode("book_not_found")
  val DuplicateBook = errorCode("duplicate_book")
  val InvalidPageRequest = errorCode("invalid_page_request")
  val UnknownError = errorCode("unknown_error")
}
