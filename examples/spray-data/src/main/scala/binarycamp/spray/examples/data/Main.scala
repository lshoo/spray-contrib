package binarycamp.spray.examples.data

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import binarycamp.spray.common._
import binarycamp.spray.data._
import binarycamp.spray.data.PageDirectives.MalformedPageRequestRejection
import scala.concurrent.duration._
import scala.util.control.NonFatal
import spray.http.{ Language, LanguageRange, StatusCodes, Uri }
import spray.routing._
import spray.util._

object Main extends App with SimpleRoutingApp with LanguageDirectives with PageDirectives with JsonSupport {
  implicit val system = ActorSystem("example")

  import system.dispatcher

  implicit val timeout = Timeout(1.seconds)

  implicit val errorTranslator = new ToMarshallableErrorTranslator {
    val ms = new ResourceMessageSource("Messages")

    private val links = LinkProvider[Error] { error ⇒
      Some(Seq(Link(Uri("http://localhost:8080/errors/" + error.code.id))))
    }

    private def format(fmt: String, args: Seq[Any]): String = String.format(fmt, args.map(_.asInstanceOf[AnyRef]): _*)

    override def apply(error: Error, languages: Seq[LanguageRange]): MarshallableError =
      ms(error.code.id, error.args, LanguagePolicy.Default(languages)) match {
        case Right(msg) ⇒ MarshallableError(error.code.id, msg, None, links(error))
        case Left(_)    ⇒ MarshallableError(error.code.id, error.code.id, None, links(error))
      }
  }

  implicit val statusCodeMapper = ErrorToStatusCodeMapper {
    case ErrorCode(Errors.BookNotFound)       ⇒ StatusCodes.NotFound
    case ErrorCode(Errors.DuplicateBook)      ⇒ StatusCodes.BadRequest
    case ErrorCode(Errors.InvalidPageRequest) ⇒ StatusCodes.BadRequest
  }

  val exceptionHandler = ExceptionHandler {
    case NonFatal(_) ⇒ completeWithLanguage(Error(Errors.UnknownError))
  }

  val rejectionHandler = RejectionHandler {
    case MalformedPageRequestRejection(_, _) :: _ ⇒ completeWithLanguage(Error(Errors.InvalidPageRequest))
    case _                                        ⇒ completeWithLanguage(Error(Errors.UnknownError))
  }

  val handleErrors = handleExceptions(exceptionHandler) & handleRejections(rejectionHandler)

  startServer("localhost", 8080) {
    handleErrors {
      pathPrefix("api") {
        pathPrefix("books") {
          pathEnd {
            getWithPageRequest { pageRequest ⇒
              completeWithLanguage(getBooks(pageRequest))
            } ~ post {
              entity(as[NewBook]) { book ⇒
                completeWithLanguage(addBook(book))
              }
            }
          } ~ pathPrefix(Segment) { bookId ⇒
            completeWithLanguage(getBook(bookId))
          }
        }
      } ~ get {
        pathEndOrSingleSlash {
          getFromResource("index.html")
        } ~ getFromResourceDirectory("")
      }
    }
  }

  val service = system.actorOf(Service.props)

  def getBook(bookId: String) =
    service.ask(Service.GetBook(bookId)).mapTo[Result[Book]]

  def addBook(book: NewBook) =
    service.ask(Service.AddBook(book)).mapTo[Result[Book]].map(_.withStatusCode(StatusCodes.Created))

  def getBooks(pageRequest: Option[PageRequest]) =
    service.ask(Service.GetBooks(pageRequest)).mapTo[Result[Page[Book]]].map { result ⇒
      result.map(_.asCollectionResource(Uri("http://localhost:8080/api/books")))
    }

  sys.addShutdownHook(system.shutdown())
}
