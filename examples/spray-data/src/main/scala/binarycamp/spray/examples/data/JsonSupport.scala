package binarycamp.spray.examples.data

import binarycamp.spray.common.ErrorMarshallers
import binarycamp.spray.data.DataSprayJsonFormats
import spray.httpx.SprayJsonSupport

trait JsonSupport extends DataSprayJsonFormats with ErrorMarshallers with SprayJsonSupport {
  implicit val bookJsonFormat = jsonFormat3(Book)
  implicit val newBookJsonFormat = jsonFormat2(NewBook)
}
