package binarycamp.spray.oauth2.server

import binarycamp.spray.common.{ ErrorCodeRegistry, ErrorGroup }

object Errors extends ErrorCodeRegistry {
  sealed abstract class OAuth2Error(code: String) extends ErrorGroup

  case object InvalidRequestGroup extends OAuth2Error("invalid_request")
  case object InvalidClientGroup extends OAuth2Error("invalid_client")
  case object InvalidGrantGroup extends OAuth2Error("invalid_grant")
  case object UnauthorizedClientGroup extends OAuth2Error("unauthorized_client")
  case object AccessDeniedGroup extends OAuth2Error("access_denied")
  case object UnsupportedResponseTypeGroup extends OAuth2Error("unsupported_response_type")
  case object UnsupportedGrantTypeGroup extends OAuth2Error("unsupported_grant_type")
  case object InvalidScopeGroup extends OAuth2Error("invalid_scope")
  case object ServerErrorGroup extends OAuth2Error("server_error")
  case object TemporarilyUnavailableGroup extends OAuth2Error("temporarily_unavailable")

  // format: OFF
  val InvalidFormData            = errorCode("InvalidFormData"            memberOf InvalidRequestGroup)
  val FragmentNotAllowed         = errorCode("FragmentNotAllowed"         memberOf InvalidRequestGroup)
  val MissingRequiredParameter   = errorCode("MissingRequiredParameter"   memberOf InvalidRequestGroup)
  val DuplicateParameter         = errorCode("DuplicateParameter"         memberOf InvalidRequestGroup)
  val ClientCredentialsInReqBody = errorCode("ClientCredentialsInReqBody" memberOf InvalidRequestGroup)
  val ClientAuthenticationFailed = errorCode("ClientAuthenticationFailed" memberOf InvalidClientGroup)
  val ClientMustAuthenticate     = errorCode("ClientMustAuthenticate"     memberOf InvalidClientGroup)
  val UnknownClient              = errorCode("UnknownClient"              memberOf InvalidClientGroup)
  val UnspecifiedClient          = errorCode("UnspecifiedClient"          memberOf InvalidClientGroup)
  val InvalidRedirectEndpoint    = errorCode("InvalidRedirectEndpoint"    memberOf InvalidRequestGroup)
  val EmptyScope                 = errorCode("EmptyScope"                 memberOf InvalidScopeGroup)
  val UnsupportedScope           = errorCode("UnsupportedScope"           memberOf InvalidScopeGroup)
  val UserHasDeniedAccess        = errorCode("UserHasDeniedAccess"        memberOf AccessDeniedGroup)
  val TokenCreationFailed        = errorCode("TokenCreationFailed"        memberOf ServerErrorGroup)
  val UserMustAuthenticate       = errorCode("UserMustAuthenticate"       memberOf ServerErrorGroup)
  val UnsupportedGrantType       = errorCode("UnsupportedGrantType"       memberOf UnsupportedGrantTypeGroup)
  val ServerError                = errorCode("ServerError"                memberOf ServerErrorGroup)
  val UnsupportedResponseType    = errorCode("UnsupportedResponseType"    memberOf UnsupportedResponseTypeGroup)
}
