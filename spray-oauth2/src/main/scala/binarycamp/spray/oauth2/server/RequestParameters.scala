package binarycamp.spray.oauth2.server

import scala.annotation.tailrec

case class RequestParameters(parameters: Map[String, String], duplicateParameters: Set[String]) {
  def apply(key: String): String = parameters(key)

  def get(key: String): Option[String] = parameters.get(key)

  def contains(key: String): Boolean = parameters.contains(key)

  def filter(p: String ⇒ Boolean): RequestParameters = copy(parameters = parameters.filter(kv ⇒ p(kv._1)))

  def filterMissing(keys: String*): Set[String] = Set(keys: _*).diff(parameters.keySet)

  def hasDuplicateParameters: Boolean = !duplicateParameters.isEmpty

  def isDuplicate(key: String): Boolean = duplicateParameters.contains(key)

  def filterDuplicate(keys: String*): Set[String] = Set(keys: _*).filter(duplicateParameters.contains(_))
}

object RequestParameters {
  def apply(multiMap: Map[String, List[String]]): RequestParameters = {
    multiMap.filter(_._2.exists(!_.trim.isEmpty)).partition(_._2.size == 1) match {
      case (valid, duplicate) ⇒ RequestParameters(valid.mapValues(_.head), duplicate.keySet)
    }
  }

  def apply(fields: Seq[(String, String)]): RequestParameters = {
    @tailrec
    def append(params: Map[String, String], dups: Set[String], fields: Seq[(String, String)]): RequestParameters = {
      if (fields.isEmpty) RequestParameters(params, dups)
      else {
        val (key, value) = fields.head
        val p = if (params.contains(key)) params - key else if (dups.contains(key)) params else params + (key -> value)
        val d = if (params.contains(key)) dups + key else dups
        append(p, d, fields.tail)
      }
    }
    append(Map.empty, Set.empty, fields)
  }
}
