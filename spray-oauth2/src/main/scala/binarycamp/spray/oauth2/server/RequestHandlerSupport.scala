package binarycamp.spray.oauth2.server

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.RequestResponseElements._
import scala.concurrent.ExecutionContext
import shapeless.{ ::, HNil }
import spray.http.Uri
import spray.routing.Directives._
import spray.routing._

trait RequestHandlerSupport {
  def clientContext(clientService: ClientService,
                    params: RequestParameters)(implicit ec: ExecutionContext): Directive[Client :: Uri :: HNil] = {
    val duplicates = params.filterDuplicate(ClientId, RedirectionUri, State)
    if (!duplicates.isEmpty) reject(ErrorRejection(DuplicateParameter, duplicates))
    else {
      if (!params.contains(ClientId)) reject(ErrorRejection(MissingRequiredParameter, ClientId))
      else client(clientService, params(ClientId)).flatMap { client ⇒
        redirectionUri(client, params.get(RedirectionUri)).flatMap { redirectionUri ⇒
          hprovide(client :: redirectionUri :: HNil)
        }
      }
    }
  }

  private def client(clientService: ClientService, id: String)(implicit ec: ExecutionContext): Directive1[Client] =
    onSuccess(clientService(id)).flatMap {
      case Some(client) ⇒ provide(client)
      case None         ⇒ reject(ErrorRejection(UnknownClient, id))
    }

  private def redirectionUri(client: Client, redirectionUri: Option[String]): Directive1[Uri] =
    client.validateRedirectionUri(redirectionUri) match {
      case Right(uri)      ⇒ provide(uri)
      case Left(rejection) ⇒ reject(rejection)
    }

  def optionalClient(clientService: ClientService,
                     params: RequestParameters)(implicit ec: ExecutionContext): Directive1[Option[Client]] =
    if (params.contains(ClientId)) {
      val clientId = params(ClientId)
      onSuccess(clientService(clientId)).flatMap {
        case Some(client) ⇒ provide(Some(client))
        case None         ⇒ reject(ErrorRejection(UnknownClient, clientId))
      }
    } else provide(None)
}
