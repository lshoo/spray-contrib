package binarycamp.spray.oauth2.server

import binarycamp.spray.common.ErrorRejection
import binarycamp.spray.oauth2.server.Errors._
import binarycamp.spray.oauth2.server.ScopePolicies._
import scala.util.control.NonFatal
import spray.http.Uri
import spray.routing.Rejection

case class Client(
    id: String,
    name: String,
    clientType: ClientType,
    hasCredentials: Boolean,
    scopes: Set[String],
    defaultScopes: Option[Set[String]],
    scopePolicy: ScopePolicy,
    redirectionUris: Seq[Uri],
    needsApproval: Boolean) {

  require(isConfidential || !redirectionUris.isEmpty, "Public clients must register at least one redirection URI")
  require(isPublic || hasCredentials, "Confidential clients require authentication")
  require(defaultScopes.isEmpty || defaultScopes.get.subsetOf(scopes), "Default scopes must be a subset of all scopes")
  redirectionUris.foreach { uri ⇒
    require(isValid(uri), s"Redirection URI ${uri.toString()} must be an absolute URI with no fragment component")
  }

  def isConfidential: Boolean = clientType == ClientTypes.Confidential

  def isPublic: Boolean = clientType == ClientTypes.Public

  def isValid(uri: Uri): Boolean = uri.isAbsolute && uri.fragment.isEmpty

  private val registeredUris = redirectionUris.map(_.toString())

  def validateRedirectionUri(redirectionUri: Option[String]): Either[Rejection, Uri] =
    redirectionUri match {
      case Some(uri) ⇒ validateRedirectionUri(uri)
      case None      ⇒ defaultRedirectionUri
    }

  private def validateRedirectionUri(redirectionUri: String): Either[Rejection, Uri] =
    (for {
      uri ← try { Some(Uri(redirectionUri)) } catch { case NonFatal(e) ⇒ None }
      if isValid(uri)
      if isConfidential && redirectionUris.isEmpty | registeredUris.exists(redirectionUri.startsWith)
    } yield uri).toRight(ErrorRejection(InvalidRedirectEndpoint))

  private def defaultRedirectionUri: Either[Rejection, Uri] =
    if (redirectionUris.size == 1) Right(redirectionUris.head) else Left(ErrorRejection(InvalidRedirectEndpoint))

  def validateScopes(ids: Option[Set[String]]): Either[Rejection, Set[String]] = scopePolicy match {
    case FailOnInvalid    ⇒ validateScopes(ids.orElse(defaultScopes).getOrElse(Set()))
    case DropInvalid      ⇒ validateScopes(ids.map(_.intersect(scopes)).filter(!_.isEmpty).orElse(defaultScopes).getOrElse(Set()))
    case AlwaysUseDefault ⇒ validateScopes(defaultScopes.getOrElse(Set()))
  }

  private def validateScopes(ids: Set[String]): Either[Rejection, Set[String]] =
    if (ids.isEmpty) Left(ErrorRejection(EmptyScope))
    else {
      val unknown = ids.diff(scopes)
      if (unknown.isEmpty) Right(ids) else Left(ErrorRejection(UnsupportedScope, unknown.toSeq))
    }
}

sealed trait ClientType

object ClientTypes {
  case object Confidential extends ClientType
  case object Public extends ClientType

  def parse(value: String): ClientType = value.toLowerCase match {
    case "confidential" ⇒ Confidential
    case "public"       ⇒ Public
    case _              ⇒ throw new IllegalClientTypeException(value)
  }
}

class IllegalClientTypeException(value: String) extends RuntimeException(s"Illegal ClientType $value")

sealed trait ScopePolicy

object ScopePolicies {
  case object FailOnInvalid extends ScopePolicy
  case object DropInvalid extends ScopePolicy
  case object AlwaysUseDefault extends ScopePolicy

  def parse(value: String): ScopePolicy = value.toLowerCase match {
    case "fail-on-invalid"    ⇒ FailOnInvalid
    case "drop-invalid"       ⇒ DropInvalid
    case "always-use-default" ⇒ AlwaysUseDefault
    case _                    ⇒ throw new IllegalScopePolicyException(value)
  }
}

class IllegalScopePolicyException(value: String) extends RuntimeException(s"Illegal ScopePolicy $value")
