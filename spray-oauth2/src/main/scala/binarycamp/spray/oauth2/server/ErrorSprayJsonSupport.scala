package binarycamp.spray.oauth2.server

import binarycamp.spray.common.DefaultSprayJsonFormats
import binarycamp.spray.data.MarshallableError
import spray.httpx.SprayJsonSupport
import spray.json._

trait ErrorSprayJsonSupport extends DefaultSprayJsonFormats with SprayJsonSupport {
  implicit val ErrorJsonFormat = lift(new RootJsonWriter[MarshallableError] {
    override def write(error: MarshallableError): JsValue =
      JsObject(field("error", error.code) ++ field("error_description", error.message))
  })
}

object ErrorSprayJsonSupport extends ErrorSprayJsonSupport
