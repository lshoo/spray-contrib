package binarycamp.spray.oauth2.server

import binarycamp.spray.pimp._
import com.typesafe.config.Config
import scala.collection.JavaConversions._

case class Scope(id: String, title: String, description: Option[String])

object Scope {
  def apply(id: String, title: String): Scope = Scope(id, title, None)
  def apply(id: String, title: String, description: String): Scope = Scope(id, title, Some(description))
}

object Scopes {
  implicit def default(implicit settings: ServerSettings): Set[Scope] = apply(settings.scopes)

  def apply(c: Config): Set[Scope] =
    (for {
      id ← c.root.unwrapped.keySet.toSeq
      scope = c.getConfig(id)
      title = scope.getString("title")
      description = scope.getOptString("description")
    } yield Scope(id, title, description)).toSet
}
