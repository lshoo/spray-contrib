package binarycamp.spray.oauth2.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import scala.collection.immutable.SortedSet
import spray.http.CacheDirectives._
import spray.http.ContentTypes._
import spray.http.HttpHeaders._
import spray.http.StatusCodes._
import spray.http._
import spray.httpx.marshalling._
import spray.httpx.unmarshalling._
import spray.json._
import spray.routing.Directives._
import spray.testkit.ScalatestRouteTest

class TokenSprayJsonSupportSpec extends FlatSpec with ScalatestRouteTest with TokenSprayJsonSupport {
  val t1 = TokenResponse(BearerToken("mF_9.B5f-4.1JqM"), None, 3600, None)
  val json1 = """
                |{
                |  "access_token": "mF_9.B5f-4.1JqM",
                |  "token_type": "Bearer",
                |  "expires_in": 3600
                |}
              """.stripMargin

  val t2 = TokenResponse(BearerToken("mF_9.B5f-4.1JqM"), None, 3600, Some(SortedSet("s1", "s2", "s3")))
  val json2 = """
                |{
                |  "access_token": "mF_9.B5f-4.1JqM",
                |  "token_type": "Bearer",
                |  "expires_in": 3600,
                |  "scope": "s1 s2 s3"
                |}
              """.stripMargin

  val t3 = TokenResponse(BearerToken("mF_9.B5f-4.1JqM"), Some("tGzv3JOkF0XG5Qx2TlKWIA"), 3600, None)
  val json3 = """
                |{
                |  "access_token": "mF_9.B5f-4.1JqM",
                |  "token_type": "Bearer",
                |  "expires_in": 3600,
                |  "refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA"
                |}
              """.stripMargin

  val t4 = TokenResponse(BearerToken("mF_9.B5f-4.1JqM"), Some("tGzv3JOkF0XG5Qx2TlKWIA"), 3600,
    Some(SortedSet("s1", "s2", "s3")))
  val json4 = """
                |{
                |  "access_token": "mF_9.B5f-4.1JqM",
                |  "token_type": "Bearer",
                |  "expires_in": 3600,
                |  "refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA",
                |  "scope": "s1 s2 s3"
                |}
              """.stripMargin

  val t5 = TokenResponse(MacToken("SlAV32hkKG", "adijq39jdlaska9asud", "hmac-sha-256"),
    Some("8xLOxBtZp8"), 3600, Some(SortedSet("s1", "s2", "s3")))
  val json5 = """
                |{
                |  "access_token":"SlAV32hkKG",
                |  "token_type":"mac",
                |  "expires_in":3600,
                |  "refresh_token":"8xLOxBtZp8",
                |  "mac_key":"adijq39jdlaska9asud",
                |  "mac_algorithm":"hmac-sha-256",
                |  "scope": "s1 s2 s3"
                |}
              """.stripMargin

  def checkMarshalledToken(token: TokenResponse, json: String) = {
    val Right(HttpEntity.NonEmpty(`application/json`, data)) = marshal(token)
    data.asString.parseJson should be(json.parseJson)
  }

  def checkUnmarshalledToken(token: TokenResponse, json: String) =
    HttpEntity(`application/json`, json).as[TokenResponse] should be(Right(token))

  "TokenSprayJsonSupport" should "marshal a Bearer access token with no refresh token and no scopes" in {
    checkMarshalledToken(t1, json1)
  }

  it should "marshal a Bearer access token with scopes and no refresh token" in {
    checkMarshalledToken(t2, json2)
  }

  it should "marshal a Bearer access token with refresh token and no scopes" in {
    checkMarshalledToken(t3, json3)
  }

  it should "marshal a Bearer access token with refresh token and scopes" in {
    checkMarshalledToken(t4, json4)
  }

  it should "marshal a Mac access token with refresh token and scopes" in {
    checkMarshalledToken(t5, json5)
  }

  it should "unmarshal a Bearer access token with no refresh token and no scopes" in {
    checkUnmarshalledToken(t1, json1)
  }

  it should "unmarshal a Bearer access token with scopes and no refresh token" in {
    checkUnmarshalledToken(t2, json2)
  }

  it should "unmarshal a Bearer access token with refresh token and no scopes" in {
    checkUnmarshalledToken(t3, json3)
  }

  it should "unmarshal a Bearer access token with refresh token and scopes" in {
    checkUnmarshalledToken(t4, json4)
  }

  it should "unmarshal a Mac access token with refresh token and scopes" in {
    checkUnmarshalledToken(t5, json5)
  }

  it should "respond with status code 200, Content-Type: application/json, Cache-Control: no-store and Pragma: no-cache" in {
    Get("/") ~> { complete(t1) } ~> check {
      status should be(OK)
      contentType should be(`application/json`)
      header[`Cache-Control`] should be(Some(`Cache-Control`(`no-store`)))
      header("Pragma") should be(Some(RawHeader("Pragma", "no-cache")))
    }
  }
}
