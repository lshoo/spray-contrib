package binarycamp.spray.pimp

import scala.reflect._

class PimpedSeq[A](val seq: Seq[A]) extends AnyVal {
  def filterByType[B <: A: ClassTag]: Seq[B] = {
    val erasure = classTag.runtimeClass
    seq.filter(item ⇒ erasure.isInstance(item)).asInstanceOf[Seq[B]]
  }
}
