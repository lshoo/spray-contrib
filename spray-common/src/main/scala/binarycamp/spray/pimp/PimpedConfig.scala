package binarycamp.spray.pimp

import com.typesafe.config.Config
import scala.collection.JavaConversions._
import spray.http.Uri
import spray.http.Uri.Path

class PimpedConfig(val config: Config) extends AnyVal {
  def getOptConfig(path: String): Option[Config] =
    if (config.hasPath(path)) Some(config.getConfig(path)) else None

  def getOptString(path: String): Option[String] =
    if (config.hasPath(path)) Some(config.getString(path)) else None

  def getOptInt(path: String): Option[Int] =
    if (config.hasPath(path)) Some(config.getInt(path)) else None

  def getOptLong(path: String): Option[Long] =
    if (config.hasPath(path)) Some(config.getLong(path)) else None

  def getStringSet(path: String): Set[String] = config.getStringList(path).toSet[String]

  def getOptStringSet(path: String): Option[Set[String]] =
    if (config.hasPath(path)) Some(config.getStringList(path).toSet[String]) else None

  def getUri(path: String): Uri = Uri(config.getString(path))

  def getUriPath(path: String): Path = Path(config.getString(path))

  def getRelativeUriPath(path: String): Path = {
    val p = getUriPath(path)
    require(!p.startsWithSlash)
    p
  }
}
