package binarycamp.spray.common

import java.util.Locale
import scala.annotation.tailrec
import spray.http.{ Language, LanguageRange, LanguageRanges }

trait LanguagePolicy extends (Seq[LanguageRange] ⇒ Seq[Locale])

object LanguagePolicy {
  def apply(f: Seq[LanguageRange] ⇒ Seq[Locale]): LanguagePolicy = new LanguagePolicy {
    override def apply(languages: Seq[LanguageRange]): Seq[Locale] = f(languages)
  }

  implicit val Default = new LanguagePolicy {
    override def apply(languages: Seq[LanguageRange]): Seq[Locale] =
      expand {
        if (languages.isEmpty) Seq(defaultLocale)
        else languages.distinct.map {
          case language: Language ⇒ asLocale(language)
          case LanguageRanges.`*` ⇒ defaultLocale
        }
      }

    private def expand(locales: Seq[Locale]): Seq[Locale] = {
      @tailrec
      def expand(acc: Seq[Locale], locales: Seq[Locale]): Seq[Locale] = {
        val parents = locales.map(drop).flatten.diff(acc)
        if (parents.isEmpty) acc
        else expand(acc ++ parents, parents)
      }

      expand(locales, locales)
    }

    private def defaultLocale: Locale = Locale.getDefault(Locale.Category.FORMAT)

    private def asLocale(range: LanguageRange): Locale = range match {
      case Language(language, country, variant, _) ⇒ new Locale(language, country, variant)
      case Language(language, country, variant)    ⇒ new Locale(language, country, variant)
      case Language(language, country)             ⇒ new Locale(language, country)
      case Language(language)                      ⇒ new Locale(language)
      case LanguageRanges.`*`                      ⇒ defaultLocale
    }

    private def drop(locale: Locale): Option[Locale] =
      if (locale.getVariant != "") Some(new Locale(locale.getLanguage, locale.getCountry))
      else if (locale.getCountry != "") Some(new Locale(locale.getLanguage))
      else None
  }
}
