package binarycamp.spray.common

import scala.language.implicitConversions

sealed case class Error(code: ErrorCode, args: Seq[Any], defaultMessage: Option[String], errors: Option[Seq[Error]]) {
  def withArgs(args: Any*): Error = copy(args = args)

  def withDefaultMessage(message: String): Error = copy(defaultMessage = Some(message))

  def withErrors(errors: Seq[Error]): Error = copy(errors = Some(errors))
}

object Error {
  def apply(code: ErrorCode, args: Any*): Error = Error(code, args, None, None)

  implicit def errorCodeToError(code: ErrorCode): Error = apply(code)
}
