package binarycamp.spray.common

import java.util.concurrent.atomic.AtomicReference
import scala.annotation.tailrec
import scala.language.implicitConversions

sealed trait ErrorCode {
  def id: String

  def group: Option[ErrorGroup]

  def hasGroup: Boolean = group.isDefined
}

object ErrorCode {
  def unapply(error: Error): Option[ErrorCode] = Some(error.code)
}

trait ErrorGroup

object ErrorGroup {
  def unapply(error: Error): Option[ErrorGroup] = error.code.group
  def unapply(code: ErrorCode): Option[ErrorGroup] = code.group
}

private[common] case class ErrorCodeImpl(id: String, group: Option[ErrorGroup]) extends ErrorCode

sealed trait ErrorCodeBuilder {
  private[common] def build: ErrorCode
}

final case class SimpleErrorCodeBuilder(id: String) extends ErrorCodeBuilder {
  def memberOf(group: ErrorGroup): ErrorCodeWithGroupBuilder = ErrorCodeWithGroupBuilder(id, group)
  override private[common] def build: ErrorCode = ErrorCodeImpl(id, None)
}

final case class ErrorCodeWithGroupBuilder(id: String, group: ErrorGroup) extends ErrorCodeBuilder {
  override private[common] def build: ErrorCode = ErrorCodeImpl(id, Some(group))
}

object ErrorCodeBuilder extends ToErrorCodeBuilderPimps

trait ToErrorCodeBuilderPimps {
  implicit def stringToErrorCodeBuilder(id: String): SimpleErrorCodeBuilder = SimpleErrorCodeBuilder(id)
  implicit def symbolToErrorCodeBuilder(symbol: Symbol): SimpleErrorCodeBuilder = SimpleErrorCodeBuilder(symbol.name)
}

trait ErrorCodeRegistry extends ToErrorCodeBuilderPimps {
  final def errorCode(builder: ErrorCodeBuilder): ErrorCode = register(builder.build)

  private[this] val registry = new AtomicReference(Map.empty[String, ErrorCode])

  @tailrec
  private def register(code: ErrorCode): ErrorCode = {
    val original = registry.get()
    require(!original.contains(code.id), s"ErrorCode ${code.id} has already been registered")
    val updated = original.updated(code.id, code)
    if (registry.compareAndSet(original, updated)) code else register(code)
  }
}
