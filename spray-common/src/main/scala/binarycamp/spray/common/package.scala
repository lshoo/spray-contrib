package binarycamp.spray

import scala.language.implicitConversions

package object common {
  type Result[T] = Either[Error, T]

  type ToStringSerializer[T] = Converter[T, String]
  type ToStringOptionSerializer[T] = Converter[T, Option[String]]
  type ParamMapWriter[T] = Converter[T, Map[String, String]]

  type Properties = Map[String, String]

  type ResolvedMessage = Either[MessageResolutionError, String]

  implicit def pimpResult[T](result: Result[T]): PimpedResult[T] = new PimpedResult[T](result)
}
