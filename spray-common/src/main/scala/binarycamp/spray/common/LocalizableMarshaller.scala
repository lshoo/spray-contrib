package binarycamp.spray.common

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success, Try }
import spray.http._
import spray.httpx.marshalling._

trait LocalizableMarshaller[-T] {
  def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T]
}

object LocalizableMarshaller
    extends BasicLocalizableMarshallers
    with MetaLocalizableMarshallers
    with LowPriorityLocalizableMarshallerImplicits {

  def apply[T](f: Seq[LanguageRange] ⇒ ToResponseMarshaller[T]): LocalizableMarshaller[T] =
    new LocalizableMarshaller[T] {
      override def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T] = f(languages)
    }
}

private[common] trait BasicLocalizableMarshallers {
  implicit def fromStatusCodeAndT[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[(StatusCode, T)] =
    LocalizableMarshaller[(StatusCode, T)] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        m(languages)(value._2, ctx.withResponseMapped { response ⇒
          response.copy(status = value._1)
        })
      }
    }

  implicit def fromStatusCodeAndHeadersAndT[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[(StatusCode, Seq[HttpHeader], T)] =
    LocalizableMarshaller[(StatusCode, Seq[HttpHeader], T)] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        m(languages)(value._3, ctx.withResponseMapped { response ⇒
          response.copy(status = value._1, headers = (value._2 ++ response.headers).toList)
        })
      }
    }
}

private[common] trait MetaLocalizableMarshallers {
  implicit def optionMarshaller[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[Option[T]] =
    LocalizableMarshaller[Option[T]] { languages ⇒
      ToResponseMarshaller {
        case (Some(value), ctx) ⇒ m(languages)(value, ctx)
        case (None, ctx)        ⇒ ctx.marshalTo(HttpResponse(StatusCodes.NotFound))
      }
    }

  implicit def eitherMarshaller[A, B](implicit ma: LocalizableMarshaller[A],
                                      mb: LocalizableMarshaller[B]): LocalizableMarshaller[Either[A, B]] =
    LocalizableMarshaller[Either[A, B]] { languages ⇒
      ToResponseMarshaller {
        case (Left(a), ctx)  ⇒ ma(languages)(a, ctx)
        case (Right(b), ctx) ⇒ mb(languages)(b, ctx)
      }
    }

  implicit def futureMarshaller[T](implicit m: LocalizableMarshaller[T],
                                   ec: ExecutionContext): LocalizableMarshaller[Future[T]] =
    LocalizableMarshaller[Future[T]] { languages ⇒
      ToResponseMarshaller { (value, ctx) ⇒
        value.onComplete {
          case Success(v)     ⇒ m(languages)(v, ctx)
          case Failure(error) ⇒ ctx.handleError(error)
        }
      }
    }

  implicit def tryMarshaller[T](implicit m: LocalizableMarshaller[T]): LocalizableMarshaller[Try[T]] =
    LocalizableMarshaller[Try[T]] { languages ⇒
      ToResponseMarshaller {
        case (Success(value), ctx) ⇒ m(languages)(value, ctx)
        case (Failure(t), ctx)     ⇒ ctx.handleError(t)
      }
    }
}

private[common] trait LowPriorityLocalizableMarshallerImplicits {
  implicit def defaultLocalizableMarshaller[T](implicit m: ToResponseMarshaller[T]): LocalizableMarshaller[T] =
    new LocalizableMarshaller[T] {
      override def apply(languages: Seq[LanguageRange]): ToResponseMarshaller[T] = m
    }
}
