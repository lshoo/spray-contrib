package binarycamp.spray.common

import spray.http.{ Language, StatusCodes }

object Errors extends ErrorCodeRegistry {
  case object Group extends ErrorGroup

  val ErrorCode1 = errorCode("Error1")
  val ErrorCode2 = errorCode("Error2" memberOf Group)
  val ErrorCode3 = errorCode("Error3")

  val EN = Language("en")
  val DE = Language("de")

  val Error1 = Error(ErrorCode1)
  val Error2 = Error(ErrorCode2)
  val Error3 = Error(ErrorCode3)

  val Error1Message_EN = "Error 1"
  val Error1Message_DE = "Fehler 1"
  val Error2Message_EN = "Error 2"
  val Error2Message_DE = "Fehler 2"

  val Error1StatusCode = StatusCodes.InternalServerError
  val Error2StatusCode = StatusCodes.NotImplemented
}
