package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import Errors._

class ErrorCodeBuilderSpec extends FlatSpec {
  "ErrorCodeBuilder" should "build an ErrorCode with the same id and group" in {
    SimpleErrorCodeBuilder("Error1").build should be(ErrorCode1)
    ErrorCodeWithGroupBuilder("Error2", Group).build should be(ErrorCode2)
  }
}
