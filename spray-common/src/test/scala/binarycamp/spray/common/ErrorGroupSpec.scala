package binarycamp.spray.common

import org.scalatest.FlatSpec
import Errors._

class ErrorGroupSpec extends FlatSpec {
  "ErrorGroup" should "extract ErrorGroup from an Error" in {
    Error2 match {
      case ErrorGroup(Group) ⇒
      case _                 ⇒ fail("failed to extract ErrorGroup")
    }
  }

  it should "extract ErrorGroup from an ErrorCode" in {
    ErrorCode2 match {
      case ErrorGroup(Group) ⇒
      case _                 ⇒ fail("failed to extract ErrorGroup")
    }
  }
}
