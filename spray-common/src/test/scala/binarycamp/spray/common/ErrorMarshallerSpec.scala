package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import scala.concurrent.Future
import spray.http.{ StatusCode, StatusCodes }
import spray.httpx.marshalling.ToResponseMarshaller
import spray.routing.Directives
import spray.testkit.ScalatestRouteTest
import Errors._

class ErrorMarshallerSpec extends FlatSpec with ScalatestRouteTest with Directives with ErrorMarshallingSupport
    with ErrorMarshallers {

  def test1[T](t: T)(implicit m: ToResponseMarshaller[T]) = test(t, Error1Message_EN, Error1StatusCode)

  def test2[T](t: T)(implicit m: ToResponseMarshaller[T]) = test(t, Error2Message_EN, Error2StatusCode)

  def test[T](t: T, message: String, statusCode: StatusCode)(implicit m: ToResponseMarshaller[T]) =
    Get() ~> complete(t) ~> check {
      responseAs[String] should be(message)
      status should be(statusCode)
    }

  "ErrorMarshaller" should "marshal Error" in {
    test1(Error1)
    test2(Error2)
  }

  it should "marshal Option[Error]" in {
    test1(Some(Error1))
    test2(Some(Error2))
  }

  it should "marshall Future[Error]" in {
    test1(Future.successful(Error1))
    test2(Future.successful(Error2))
  }

  it should "marshall Result[String]" in {
    test1(result(Error1))
    test2(result(Error2))

  }

  it should "marshall Future[Result[String]]" in {
    test1(Future.successful(result(Error1)))
    test2(Future.successful(result(Error2)))
  }

  it should "marshall Future[Result[String]].map(_.withStatusCode(StatusCode))" in {
    test1(Future.successful(result(Error1)).map(_.withStatusCode(StatusCodes.OK)))
    test2(Future.successful(result(Error2)).map(_.withStatusCode(StatusCodes.OK)))
  }

  it should "fallback to InternalServerError when error to status code mapping is not defined" in {
    test(Error3, ErrorCode3.id, StatusCodes.InternalServerError)
  }
}
