package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import Errors._

class ErrorCodeImplSpec extends FlatSpec {
  "ErrorCodeImpl" should "return false on hasGroup when group is empty" in {
    ErrorCode1.hasGroup should be(false)
  }

  it should "return true on hasGroup when group is defined" in {
    ErrorCode2.hasGroup should be(true)
  }
}
