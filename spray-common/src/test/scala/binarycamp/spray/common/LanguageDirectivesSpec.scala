package binarycamp.spray.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import scala.concurrent.Future
import spray.http._
import spray.http.StatusCodes.{ Created, OK }
import spray.testkit.ScalatestRouteTest
import Errors._

class LanguageDirectivesSpec extends FlatSpec with ScalatestRouteTest with LanguageDirectives
    with ErrorMarshallingSupport with ErrorMarshallers {

  def test1[T](t: T, statusCode: StatusCode = Error1StatusCode)(implicit m: LocalizableMarshaller[T]) = {
    test(t, Error1Message_EN, statusCode)
    test(t, Error1Message_EN, statusCode, EN)
    test(t, Error1Message_DE, statusCode, DE)
  }

  def test2[T](t: T, statusCode: StatusCode = Error2StatusCode)(implicit m: LocalizableMarshaller[T]) = {
    test(t, Error2Message_EN, statusCode)
    test(t, Error2Message_EN, statusCode, EN)
    test(t, Error2Message_DE, statusCode, DE)
  }

  def test[T](t: T, message: String, statusCode: StatusCode)(implicit m: LocalizableMarshaller[T]) =
    Get() ~> completeWithLanguage(t) ~> check {
      responseAs[String] should be(message)
      status should be(statusCode)
    }

  def test[T](t: T, message: String, statusCode: StatusCode, language: Language)(implicit m: LocalizableMarshaller[T]) =
    Get() ~> addHeader(HttpHeaders.`Accept-Language`(language)) ~> completeWithLanguage(t) ~> check {
      responseAs[String] should be(message)
      status should be(statusCode)
    }

  "LanguageDirectives" should "marshal Error" in {
    test1(Error1)
    test2(Error2)
  }

  it should "marshal Option[Error]" in {
    test1(Some(Error1))
    test2(Some(Error2))
  }

  it should "marshall Future[Error]" in {
    test1(Future.successful(Error1))
    test2(Future.successful(Error2))
  }

  it should "marshall Result[String]" in {
    test1(result(Error1))
    test2(result(Error2))
  }

  it should "marshall Future[Result[String]]" in {
    test1(Future.successful(result(Error1)))
    test2(Future.successful(result(Error2)))
  }

  it should "marshall (StatusCode, Future[Result[String]])" in {
    test1((OK, Future.successful(result(Error1))), OK)
    test2((OK, Future.successful(result(Error2))), OK)
  }

  it should "marshall Future[Result[String]].map(_.withStatusCode(StatusCode))" in {
    test1(Future.successful(result(Error1)).map(_.withStatusCode(OK)))
    test2(Future.successful(result(Error2)).map(_.withStatusCode(OK)))
  }

  it should "marshall successful Result with overridden status code" in {
    test(Right("Ok").withStatusCode(Created), "Ok", Created)
  }
}
