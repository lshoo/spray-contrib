package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import binarycamp.spray.common.ParamMapWriter
import PageRequest._

class PageRequestParamMapWriterSpec extends FlatSpec {
  val sort = Sort("p1" -> Some(Order.Asc), "p2" -> Some(Order.Desc), "p3" -> None)
  val filter = Filter("expr")

  "PageRequestParamMapWriter" should "serialize PageRequest(index, None, None, None)" in {
    val result = ParamMapWriter.write(PageRequest(1, None, None, None))(PageRequestParamMapWriter)
    result should be(Map(IndexParam -> "1"))
  }

  it should "serialize PageRequest(index, Some(size), None, None)" in {
    val result = ParamMapWriter.write(PageRequest(1, Some(5), None, None))(PageRequestParamMapWriter)
    result should be(Map(IndexParam -> "1", SizeParam -> "5"))
  }

  it should "serialize PageRequest(index, Some(size), Some(sort), None)" in {
    val result = ParamMapWriter.write(PageRequest(1, Some(5), Some(sort), None))(PageRequestParamMapWriter)
    result should be(Map(IndexParam -> "1", SizeParam -> "5", SortParam -> "p1:asc,p2:desc,p3"))
  }

  it should "serialize PageRequest(index, Some(size), None, Some(filter))" in {
    val result = ParamMapWriter.write(PageRequest(1, Some(5), None, Some(filter)))(PageRequestParamMapWriter)
    result should be(Map(IndexParam -> "1", SizeParam -> "5", FilterParam -> "expr"))
  }

  it should "serialize PageRequest(index, Some(size), Some(sort), Some(filter))" in {
    val result = ParamMapWriter.write(PageRequest(1, Some(5), Some(sort), Some(filter)))(PageRequestParamMapWriter)
    result should be(Map(IndexParam -> "1", SizeParam -> "5", SortParam -> "p1:asc,p2:desc,p3", FilterParam -> "expr"))
  }
}
