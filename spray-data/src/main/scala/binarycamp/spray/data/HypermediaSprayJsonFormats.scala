package binarycamp.spray.data

import binarycamp.spray.common.DefaultSprayJsonFormats
import binarycamp.spray.pimp.json._
import spray.http.Uri
import spray.json._
import DefaultFieldNames._

trait HypermediaSprayJsonFormats extends DefaultSprayJsonFormats with HypermediaPimps {
  implicit object LinksJsonFormat extends JsonFormat[Seq[Link]] {
    override def read(json: JsValue): Seq[Link] = json.asJsObject.fields.map(readLink).toSeq

    private def readLink(field: JsField): Link =
      Link(field._1, fromField[Uri](field._2, LinkHref), fromField[Option[String]](field._2, LinkTitle))

    override def write(links: Seq[Link]): JsValue = JsObject(links.map(writeLink): _*)

    private def writeLink(link: Link): JsField =
      link.relation -> JsObject(field(LinkHref, link.href) ++ field(LinkTitle, link.title))
  }

  implicit def withHypermediaJsonFormat[T](implicit ef: JsonFormat[T],
                                           lf: JsonFormat[Seq[Link]]): RootJsonFormat[WithHypermedia[T]] =
    new RootJsonFormat[WithHypermedia[T]] {
      override def read(json: JsValue): WithHypermedia[T] =
        WithHypermedia(ef.read(json), fromField[Option[Seq[Link]]](json, Links))

      override def write(value: WithHypermedia[T]): JsValue =
        ef.write(value.entity).asJsObject.merge(field(Links, value.links))
    }
}
