package binarycamp.spray.data

import shapeless._

trait BiPrepend[P <: HList, S <: HList] {
  type Out <: HList
  def prepend(p: P, s: S): Out
  def split(o: Out): (P, S)
}

trait LowPriorityBiPrepend {
  type Aux[P <: HList, S <: HList, Out0 <: HList] = BiPrepend[P, S] { type Out = Out0 }

  implicit def forPHNil[P <: HList]: Aux[P, HNil.type, P] =
    new BiPrepend[P, HNil.type] {
      type Out = P
      override def prepend(p: P, s: HNil.type): Out = p
      override def split(o: Out): (P, HNil.type) = (o, HNil)
    }
}

object BiPrepend extends LowPriorityBiPrepend {
  implicit def forObjHNilS[S <: HList]: Aux[HNil.type, S, S] =
    new BiPrepend[HNil.type, S] {
      type Out = S
      override def prepend(p: HNil.type, s: S): Out = s
      override def split(o: Out): (HNil.type, S) = (HNil, o)
    }

  implicit def forHNilS[S <: HList]: Aux[HNil, S, S] =
    new BiPrepend[HNil, S] {
      type Out = S
      override def prepend(p: HNil, s: S): Out = s
      override def split(o: Out): (HNil, S) = (HNil, o)
    }

  implicit def forHList[PH, PT <: HList, S <: HList](implicit pts: BiPrepend[PT, S]): Aux[PH :: PT, S, PH :: pts.Out] =
    new BiPrepend[PH :: PT, S] {
      type Out = PH :: pts.Out
      override def prepend(p: PH :: PT, s: S): Out = p.head :: pts.prepend(p.tail, s)
      override def split(o: Out): (PH :: PT, S) = {
        val (a, b) = pts.split(o.tail)
        (o.head :: a, b)
      }
    }
}
