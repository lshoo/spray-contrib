package binarycamp.spray.data

import binarycamp.spray.common.DefaultSprayJsonFormats
import spray.json._
import DefaultFieldNames._

trait ErrorSprayJsonFormats extends DefaultSprayJsonFormats with HypermediaSprayJsonFormats {
  implicit object MarshallableErrorJsonFormat extends RootJsonFormat[MarshallableError] {
    private val errorFormat = jsonFormat(MarshallableError, ErrorCode, ErrorMessage, ErrorMessages, Links)

    override def read(json: JsValue): MarshallableError = json match {
      case JsObject(fields) if fields.contains(Error) ⇒ errorFormat.read(fields(Error))
      case _ ⇒ deserializationError(s"Expected JsObject with field $Error")
    }

    override def write(error: MarshallableError): JsValue = JsObject(field(Error, error)(errorFormat))
  }
}
