package binarycamp.spray.data

trait DataSprayJsonFormats
  extends PageSprayJsonFormats
  with HypermediaSprayJsonFormats
  with ErrorSprayJsonFormats

object DataSprayJsonFormats extends DataSprayJsonFormats
