package binarycamp.spray.data

import binarycamp.spray.common.DefaultSprayJsonFormats
import scala.util.control.NonFatal
import spray.json._
import DefaultFieldNames._
import PageRequestDeserializers._

trait PageSprayJsonFormats extends DefaultSprayJsonFormats {
  this: HypermediaSprayJsonFormats ⇒

  implicit def pageJsonFormat[T](implicit jf: JsonFormat[Seq[T]], cf: JsonFormat[Int],
                                 lf: JsonFormat[Seq[Link]] = LinksJsonFormat): RootJsonFormat[Page[T]] =
    new RootJsonFormat[Page[T]] {
      override def read(json: JsValue): Page[T] = {
        val items = fromField[Seq[T]](json, PageItems)
        val count = fromField[Option[Int]](json, TotalCount)
        val links = try fromField[Option[Seq[Link]]](json, Links) catch { case NonFatal(e) ⇒ None }

        links.flatMap(selfLink).flatMap(pageRequest) match {
          case Some(PageRequest(index, size, sort, filter)) ⇒
            val s = size.getOrElse(items.size)
            Page(items, index, s, count.getOrElse(s * index + items.size), sort, filter)
          case None ⇒ Page(items, 0, items.size, count.getOrElse(items.size))
        }
      }

      private def selfLink(links: Seq[Link]): Option[Link] = links.find(_.isSelf)

      private def pageRequest(link: Link): Option[PageRequest] =
        PageRequestDeserializers.read(link.href.query.toMultiMap) match {
          case Right(pageRequest) ⇒ pageRequest
          case Left(_)            ⇒ None
        }

      override def write(page: Page[T]): JsValue =
        JsObject(field(PageItems, page.items) ++ field(TotalCount, page.count))
    }
}
