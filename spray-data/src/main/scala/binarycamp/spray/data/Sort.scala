package binarycamp.spray.data

case class Sort(fields: Seq[(String, Option[Order])]) {
  def merge(that: Sort): Sort = Sort(fields ++ that.fields)
}

object Sort {
  def apply(): Sort = Sort(Seq.empty)
  def apply(head: (String, Option[Order]), tail: (String, Option[Order])*): Sort = Sort(head +: tail)
}

sealed trait Order

object Order {
  case object Asc extends Order
  case object Desc extends Order
}
