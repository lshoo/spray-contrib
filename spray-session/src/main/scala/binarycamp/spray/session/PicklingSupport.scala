package binarycamp.spray.session

import org.apache.commons.codec.binary.Base64
import scala.language.implicitConversions
import scala.pickling._
import scala.pickling.binary._
import spray.httpx.unmarshalling._
import spray.routing.directives.NameDeserializerReceptacle

trait PicklingSupport {
  implicit def string2PicklingAttributeAccessor(name: String): PicklingAttributeAccessor =
    PicklingAttributeAccessor(name)

  implicit def symbol2PicklingAttributeAccessor(symbol: Symbol): PicklingAttributeAccessor =
    PicklingAttributeAccessor(symbol.name)

  def picklingAttributeSerializer[A: SPickler: FastTypeTag]: ToStringSerializer[A] =
    new ToStringSerializer[A] {
      override def apply(value: A): String = Base64.encodeBase64String(value.pickle.value)
    }

  def picklingAttributeDeserializer[A: Unpickler: FastTypeTag]: FromStringDeserializer[A] =
    new FromStringDeserializer[A] {
      override def apply(value: String): Deserialized[A] = Right(BinaryPickle(Base64.decodeBase64(value)).unpickle[A])
    }
}

object PicklingSupport extends PicklingSupport

case class PicklingAttributeAccessor(name: String) {
  def pickle[A: SPickler: FastTypeTag](value: A): AttributeSerializerSetter[A] =
    AttributeSerializerSetter[A](name, value, PicklingSupport.picklingAttributeSerializer[A])

  def unpickleAs[A: Unpickler: FastTypeTag]: NameDeserializerReceptacle[A] =
    NameDeserializerReceptacle[A](name, PicklingSupport.picklingAttributeDeserializer[A])
}
